package transaction

import "math/rand"

// RandomService simulates writing to the blockchain with random events
type RandomService struct {
	Store
}

func (s *RandomService) Broadcast(txn *Transaction) error {
	txn.State = StateAwaitingConfirmation
	return nil
}

func (s *RandomService) Status(txn *Transaction) error {
	if rand.Int()%2 == 1 {
		txn.State = StateFailed
	} else {
		txn.State = StateSettled
	}

	return nil
}
