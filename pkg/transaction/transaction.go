//go:generate stringer -linecomment -type=State -output zz_state_string.go
package transaction

import "encoding/json"

type State uint8

const (
	StateCreated              State = 0 + iota // Created
	StateAwaitingConfirmation                  // Awaiting Confirmation
	StateFailed                                // Failed
	StateSettled                               // Settled
)

func (s State) MarshalJSON() ([]byte, error) {
	return json.Marshal(s.String())
}

type Transaction struct {
	UUID          string  `uri:"transaction_uuid" json:"transaction_uuid"`
	UserUUID      string  `json:"user_uuid"`
	WalletAddress string  `json:"wallet"`
	USDAmount     float64 `json:"usd"`
	BitcoinAmount float64 `json:"btc"`
	State         State   `json:"state"`
}
