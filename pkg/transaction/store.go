package transaction

// Store persists all transaction information
type Store interface {
	// Create generates a unique transaction uuid and stores the current state of the transaction
	Create(txn *Transaction) error

	// Update stores the current state of the transaction
	Update(txn *Transaction) error

	// Get updates the transaction to reflect the current state of the transaction in the store. Returns true if the
	// transaction exists.
	Get(txn *Transaction) (bool, error)
}
