package transaction

// Service transaction supports storing transactions as well as writing them to the blockchain
type Service interface {
	Store

	// Broadcast attempts to write the transaction to the blockchain
	Broadcast(txn *Transaction) error

	// Status updates the transaction state to reflect the state of the blockchain
	Status(txn *Transaction) error
}
