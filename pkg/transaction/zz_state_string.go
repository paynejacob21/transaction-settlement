// Code generated by "stringer -linecomment -type=State -output zz_state_string.go"; DO NOT EDIT.

package transaction

import "strconv"

func _() {
	// An "invalid array index" compiler error signifies that the constant values have changed.
	// Re-run the stringer command to generate them again.
	var x [1]struct{}
	_ = x[StateCreated-0]
	_ = x[StateAwaitingConfirmation-1]
	_ = x[StateFailed-2]
	_ = x[StateSettled-3]
}

const _State_name = "CreatedAwaiting ConfirmationFailedSettled"

var _State_index = [...]uint8{0, 7, 28, 34, 41}

func (i State) String() string {
	if i >= State(len(_State_index)-1) {
		return "State(" + strconv.FormatInt(int64(i), 10) + ")"
	}
	return _State_name[_State_index[i]:_State_index[i+1]]
}
