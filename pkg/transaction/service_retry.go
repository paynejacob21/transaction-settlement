package transaction

import "github.com/sirupsen/logrus"

// RetryService will asynchronously broadcast a transaction until it has settled or the maximum attempts is reached.
type RetryService struct {
	MaxAttempts int
	Service
}

func (s *RetryService) Create(txn *Transaction) error {
	if err := s.Service.Create(txn); err != nil {
		return err
	}

	go func() {
		_txn := *txn

		go s.reconcileTransaction(&_txn)
	}()

	return nil
}

func (s *RetryService) reconcileTransaction(txn *Transaction) {
	var attempts int

	for attempts <= s.MaxAttempts {
		switch txn.State {
		case StateAwaitingConfirmation:
			logrus.Infof("[%d/%d] checking status for transaction[%s]", attempts, s.MaxAttempts, txn.UUID)
			if err := s.Status(txn); err != nil {
				logrus.Errorf("failed to get status for transaction[%s]: %s", txn.UUID, err)
			}
		case StateFailed:
			logrus.Infof("[%d/%d] failed transaction[%s]", attempts, s.MaxAttempts, txn.UUID)
			if err := s.Update(txn); err != nil {
				logrus.Errorf("failed to update transaction[%s]: %s", txn.UUID, err)
			}
			attempts += 1
		case StateSettled:
			logrus.Infof("[%d/%d] settled transaction[%s]", attempts, s.MaxAttempts, txn.UUID)
			if err := s.Update(txn); err != nil {
				logrus.Error("failed to update transaction: ", err)
			}
			attempts = s.MaxAttempts + 1
		default:
			attempts += 1
			if err := s.Broadcast(txn); err != nil {
				logrus.Errorf("failed to broadcast transaction[%s]: %s", txn.UUID, err)
			}
			if err := s.Update(txn); err != nil {
				logrus.Errorf("failed to update transaction[%s]: %s", txn.UUID, err)
			}
		}
	}
}
