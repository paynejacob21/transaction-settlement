package transaction

import (
	"sync"

	"github.com/google/uuid"
)

// MemoryStore keeps transaction states in memory, not recommended for production use
type MemoryStore struct {
	mu           sync.RWMutex
	Transactions map[string]Transaction
}

func (m *MemoryStore) Create(txn *Transaction) error {
	txn.UUID = uuid.New().String()
	m.mu.Lock()
	m.Transactions[txn.UUID] = *txn
	m.mu.Unlock()

	return nil
}

func (m *MemoryStore) Update(txn *Transaction) error {
	m.mu.Lock()
	m.Transactions[txn.UUID] = *txn
	m.mu.Unlock()

	return nil
}

func (m *MemoryStore) Get(txn *Transaction) (bool, error) {
	var exists bool

	m.mu.RLock()
	*txn, exists = m.Transactions[txn.UUID]
	m.mu.RUnlock()

	return exists, nil
}
