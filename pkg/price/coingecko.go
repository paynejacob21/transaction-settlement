package price

import (
	"encoding/json"
	"net/http"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
)

const (
	btcToUSDUrl    = "https://api.coingecko.com/api/v3/simple/price?ids=bitcoin&vs_currencies=usd"
	defaultExpires = time.Second * 120
)

type CoinGecko struct {
	mu          sync.RWMutex
	nextRefresh time.Time
	btcToUSD    float64
}

func (c *CoinGecko) USDToBTC(value float64) (float64, error) {
	var err error

	// copy the current state
	c.mu.RLock()
	refresh := time.Now().After(c.nextRefresh)
	btcToUSD := c.btcToUSD
	c.mu.RUnlock()

	// if the value is stale refresh it
	if refresh {
		c.mu.Lock()
		// check again in-case another routine already refreshed the content
		if time.Now().After(c.nextRefresh) {
			err = c.refreshRates()
		}

		// use the refreshed value
		btcToUSD = c.btcToUSD
		c.mu.Unlock()
	}

	// value is given in usd
	return value / btcToUSD, err
}

type btcToUSDResponse struct {
	Bitcoin struct {
		USD int `json:"usd"`
	} `json:"bitcoin"`
}

func (c *CoinGecko) refreshRates() error {
	resp, err := http.Get(btcToUSDUrl)
	if err != nil {
		return err
	}

	var content btcToUSDResponse
	if err = json.NewDecoder(resp.Body).Decode(&content); err != nil {
		return err
	}

	c.btcToUSD = float64(content.Bitcoin.USD)

	exp, err := time.Parse(time.RFC1123, resp.Header.Get("Expires"))
	if err != nil {
		logrus.Warning("failed to parse expires header")
		exp = time.Now().Add(defaultExpires)
	}

	c.nextRefresh = exp

	logrus.Info("refreshed btc to usd value, next refresh: ", c.nextRefresh.String())

	return nil
}
