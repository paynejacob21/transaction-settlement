package price

// Service pricing provides conversions for currency types
type Service interface {
	// USDToBTC returns equivalent value in btc of given usd
	USDToBTC(value float64) (float64, error)
}
