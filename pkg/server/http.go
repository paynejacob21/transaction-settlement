package server

import (
	"context"
	"errors"
	"net"
	"net/http"
	"time"
	"transaction-settlement/pkg/address"
	"transaction-settlement/pkg/price"
	"transaction-settlement/pkg/transaction"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type HttpServer struct {
	Listener         net.Listener
	TransactionStore transaction.Store
	PriceService     price.Service
}

func (h *HttpServer) Start(ctx context.Context) error {
	router := gin.Default()
	router.Use(h.handleError)
	router.Use(gin.Logger())

	router.POST("/transactions", h.createTransaction)
	router.GET("/transactions/:transaction_uuid", h.getTransaction)

	svr := &http.Server{Handler: router}

	go func() {
		<-ctx.Done()
		logrus.Info("stopping http server")
		shutdownCtx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		if err := svr.Shutdown(shutdownCtx); err != nil {
			logrus.Warning("http server did not shutdown cleanly in the allotted time: ", err)
		}

		cancel()
	}()

	logrus.Info("starting http server")
	if err := svr.Serve(h.Listener); err != nil && err != http.ErrServerClosed {
		return err
	}

	return nil
}

type createTransactionRequest struct {
	CustomerUUID string  `json:"customer_uuid" binding:"required"`
	AmountUSD    float64 `json:"amount" binding:"required"`
	Wallet       string  `json:"wallet" binding:"required"`
}

func (h *HttpServer) createTransaction(c *gin.Context) {
	var req createTransactionRequest

	// parse the request body
	if err := c.BindJSON(&req); err != nil {
		return
	}

	// valdiate wallet address is valid
	if err := address.Validate(req.Wallet); err != nil {
		_ = c.AbortWithError(http.StatusNotAcceptable, &gin.Error{Err: err, Type: gin.ErrorTypePublic})
		return
	}

	txn := transaction.Transaction{
		UserUUID:      req.CustomerUUID,
		WalletAddress: req.Wallet,
		USDAmount:     req.AmountUSD,
		State:         transaction.StateCreated,
	}

	// get the btc price for this instant
	if btc, err := h.PriceService.USDToBTC(txn.USDAmount); err != nil {
		_ = c.Error(err)
		return
	} else {
		txn.BitcoinAmount = btc
	}

	// store the transaction
	if err := h.TransactionStore.Create(&txn); err != nil {
		_ = c.Error(err)
		return
	}

	// respond with the transaction state
	c.JSON(http.StatusAccepted, &txn)
}

func (h *HttpServer) getTransaction(c *gin.Context) {
	var txn transaction.Transaction

	// get the transaction id from the uri
	if err := c.BindUri(&txn); err != nil {
		return
	}

	// get the transaction state from the store
	if exists, err := h.TransactionStore.Get(&txn); err != nil {
		_ = c.Error(err)
		return
	} else if !exists {
		// if the transaction does not exist inform the user
		_ = c.AbortWithError(http.StatusNotFound, &gin.Error{Err: errors.New("transaction with given id could not be found"), Type: gin.ErrorTypePublic})
		return
	}

	// respond with the transaction state
	c.JSON(http.StatusOK, txn)
}

type errorResponse struct {
	Errors []string `json:"errors"`
}

func (h *HttpServer) handleError(c *gin.Context) {
	c.Next()

	var resp errorResponse
	status := c.Writer.Status()

	// if any private errors occurred report a generic server error to the user
	for range c.Errors.ByType(gin.ErrorTypePrivate) {
		status = http.StatusInternalServerError
		break
	}
	// if this is not an error response do nothing
	if status < 399 {
		return
	}

	// gather all errors that can be displayed to the user
	for _, err := range c.Errors.ByType(gin.ErrorTypeBind) {
		resp.Errors = append(resp.Errors, err.Error())
	}
	for _, err := range c.Errors.ByType(gin.ErrorTypePublic) {
		resp.Errors = append(resp.Errors, err.Error())
	}

	c.JSON(c.Writer.Status(), resp)
}
