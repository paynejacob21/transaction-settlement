package address

import (
	"errors"
	"regexp"
)

var BTCAddressRegxp *regexp.Regexp

func init() {
	// taken from https://github.com/evzpav/simple-crypto-address-validator/blob/4b2df88c9d739d15eb77a88911a0dc1ecaeed931/main.go#L78C13-L78C49
	BTCAddressRegxp = regexp.MustCompile("^(bc1|[13])[a-zA-HJ-NP-Z0-9]{25,39}$")
}

func Validate(address string) error {
	if address == "" || len(address) < 4 {
		return errors.New("too short")
	}

	if !BTCAddressRegxp.MatchString(address) {
		return errors.New("not a valid btc wallet address")
	}

	return nil
}
