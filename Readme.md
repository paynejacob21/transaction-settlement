# Transaction Settlement

## Notes to the Reviewer
- There are no go tests, normally I would have a test suite for the service interfaces, integration tests on the endpoint, and unit test on the validators.
- There is no CI, please see https://github.com/paynejacob/speakerbob for an example of how I implement CI for a project like this.
- I made an assumption that the `customer_uuid` in the endpoint 1 description was the `user_uuid` in the endpoint 2 description.
- I am assuming broadcasting a transaction is idempotent with my error handling.
- main.go does not accept any arguments for the sake of time and convenience, please see https://github.com/paynejacob/speakerbob/tree/master/cmd for an example of how I would implement a cli interface given more time.
- The application content is currently stored in memory, in a real world scenario the contents would be persisted to disk or an external database. I focused on demonstrating how I would abstract the components of this project and provided simplistic implementations.  
- The logging is inconsistent and many info statements could be debug statements.
- This was my first project using gin now that gorilla/mux is archived.


To Run this project cd into the project directory and run.

```bash
go run cmd/main.go
```

The API will be accesible at http://localhost:8080. There is also an included postman collection for convenience.