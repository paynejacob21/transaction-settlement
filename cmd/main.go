package main

import (
	"context"
	"net"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"transaction-settlement/pkg/price"
	"transaction-settlement/pkg/server"
	"transaction-settlement/pkg/transaction"

	"github.com/sirupsen/logrus"
)

func main() {
	l, err := net.Listen("tcp", "127.0.0.1:8080")
	if err != nil {
		logrus.Fatal("failed to allocate listening port")
	}

	svr := server.HttpServer{
		Listener: l,
		TransactionStore: &transaction.RetryService{
			MaxAttempts: 3,
			Service: &transaction.RandomService{
				Store: &transaction.MemoryStore{
					Transactions: map[string]transaction.Transaction{},
				},
			},
		},
		PriceService: &price.CoinGecko{},
	}

	ctx, cancel := context.WithCancel(context.Background())

	wg := &sync.WaitGroup{}
	wg.Add(1)
	go func() {
		if err := svr.Start(ctx); err != nil {
			logrus.Warning("http server did not exit gracefully: ", err)
		}
		wg.Done()
	}()

	logrus.Info("listening on ", l.Addr().String())

	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit

	logrus.Info("shutdown signal received")

	cancel()

	wg.Wait()
}
